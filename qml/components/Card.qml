/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

Item {
    id: card

    property int cardMargin: units.gu(4)
    property string defaultIcon: "../../assets/back.svg"
    property string currentIconColor: "#f5f5f5"
    property var icons: ["symbol-lizard", "symbol-paper", "symbol-rock", "symbol-scissors", "symbol-spock"]
    property var iconWinsTo:  [["symbol-rock", "symbol-scissors"],
                               ["symbol-lizard", "symbol-scissors"],
                               ["symbol-spock", "symbol-paper"],
                               ["symbol-rock", "symbol-spock"],
                               ["symbol-lizard", "symbol-paper"]]
    property var iconLosesTo: [["symbol-spock",  "symbol-paper"],
                               ["symbol-rock", "symbol-spock"],
                               ["symbol-lizard", "symbol-scissors"],
                               ["symbol-lizard", "symbol-paper"],
                               ["symbol-rock", "symbol-scissors"]]
    property var iconColor: ["#6dff82", "#e161a0", "#86dfff", "#d2a6ff", "#f5ff82"]
    property int iteration: 0
    property int finalIconIndex
    property bool isWider: width > height

    Rectangle {
        radius: units.dp(25)
        border.color: theme.palette.normal.backgroundText
        border.width: units.dp(2)
        color: rollIcons.running
            ? "#f5f5f5"
            : currentIconColor

        anchors {
            fill: parent
            topMargin: isWider ? cardMargin / 2 : cardMargin
            bottomMargin: isWider ? cardMargin / 2 : cardMargin
            leftMargin: cardMargin / 2
            rightMargin: cardMargin / 2
        }

        MainIcon {
            width: Math.min(parent.height / 2, parent.width - cardMargin)
            height: width
            anchors.centerIn: parent
            mainIcon: defaultIcon
        }

        Row {
            spacing: isWider
                ? parent.width / 3
                : cardMargin / 2
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top
                topMargin: cardMargin / 2
            }

            MainIcon {
                id: win1
                width: Math.min(units.gu(7), card.height / 5)
            }

            MainIcon {
                id: win2
                width: Math.min(units.gu(7), card.height / 5)
            }
        }

        Row {
            spacing: isWider
                ? parent.width / 3
                : cardMargin / 2
            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: parent.bottom
                bottomMargin: cardMargin / 2
            }

            MainIcon {
                id: lose1
                width: Math.min(units.gu(7), card.height / 5)
            }

            MainIcon {
                id: lose2
                width: Math.min(units.gu(7), card.height / 5)
            }
        }
    }

    Timer {
        id: rollIcons
        interval: 1
        running: false
        repeat: true

        onTriggered: {
            iteration += 1;
            interval += iteration * 2;

            defaultIcon = "../../assets/" + icons[iteration % 5] + ".svg"

            if (iteration > 9 + finalIconIndex) {

                defaultIcon = "../../assets/" + icons[finalIconIndex] + ".svg"
                currentIconColor = iconColor[finalIconIndex]
                win1.mainIcon = "../../assets/" + iconWinsTo[finalIconIndex][0] + ".svg"
                win2.mainIcon = "../../assets/" + iconWinsTo[finalIconIndex][1] + ".svg"
                lose1.mainIcon = "../../assets/" + iconLosesTo[finalIconIndex][0] + ".svg"
                lose2.mainIcon = "../../assets/" + iconLosesTo[finalIconIndex][1] + ".svg"
                rollIcons.running = false;
                mainPage.isRolling = false;
                rollIcons.interval = 1;
                iteration = 0;
            }
        }
    }

    Connections {
        target: mainPage

        onRoll: {
            win1.mainIcon = "";
            win2.mainIcon = "";
            lose1.mainIcon = "";
            lose2.mainIcon = "";
            finalIconIndex = Math.floor(Math.random() * 5);
            rollIcons.running = true;
        }
    }
}
