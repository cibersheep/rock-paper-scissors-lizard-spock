/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

Image {
    property string mainIcon
    property string mainColor

    property int iconSize: {
        if (width > units.gu(128))
            return units.gu(512)

        if (width < units.gu(64))
            return units.gu(32)

        return units.gu(96)
    }

    width: parent.width
    height: width
    sourceSize.width: iconSize
    sourceSize.height: iconSize
    source: mainIcon
}

